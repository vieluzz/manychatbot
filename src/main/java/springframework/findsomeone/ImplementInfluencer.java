package springframework.findsomeone;



import java.util.List;

public interface ImplementInfluencer {
    public List<Influencer> AddInfluencer();
    public List<Influencer> QueryById(int id);
    public List<Influencer> QueryByAge(int age);
    public List<Influencer> Query(int age,
                                  boolean gender,
                                  int followers,
                                  String Location);
    public List<Influencer> QueryAll(int ageFrom,int ageTo,int followersFrom,int followersTo,boolean gender,String location);
    public int Count();
}


