package springframework.findsomeone;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "influencer")
public class Influencer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "fullname")
    private String fullname;

    @NotNull
    @Column(name = "gender")
    private Boolean gender;

    @NotNull
    @Column(name = "followers")
    private int followers;

    @NotNull
    @Column(name = "age")
    private int age;

    @NotNull
    @Column(name = "location")
    private String location;

    public Influencer() {

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLocation() { return location; }

    public void setLocation(String location) { this.location = location; }

    Influencer(String fullname, String username, boolean gender, int followers, int age, String location) {
        this.fullname=fullname;
        this.username=username;
        this.gender=gender;
        this.followers=followers;
        this.age=age;
        this.location=location;
    }
}
