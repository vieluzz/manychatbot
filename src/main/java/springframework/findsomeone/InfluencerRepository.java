package springframework.findsomeone;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
interface InfluencerRepository extends CrudRepository<Influencer,Long> {
        @Query
        List<Influencer> findById(int id);
        List<Influencer> findByAge(int age);
        List<Influencer> findByAgeAndGenderAndFollowersAndLocation(int age,boolean gender,int followers,String location);
        List<Influencer> findByAgeBetweenAndFollowersBetweenAndGenderAndLocation(int ageFrom,int ageTo,int followersFrom,int followersTo,boolean gender,String location);
        int countAllBy();
}
