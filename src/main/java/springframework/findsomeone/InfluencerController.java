package springframework.findsomeone;

import Config.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/")
public class InfluencerController {

    @Autowired
    InfluencerService influencerService;

    @Value("${manychat.apitoken}")
    private String author = "";

    @Value("${manychat.apiSendcontent}")
    private String Url = "";

    private String Result = new String();

    private String json;

    private static ResponseFormat responseFormat = new ResponseFormat();

    private static final String MALE = "Male";

    private static final String FEMALE = "Female";

    private static Subcribers subcribers;

    private static List<Influencer> influencers;

    private boolean Gender;

    private static final Logger logger = LoggerFactory.getLogger(InfluencerController.class);

    @RequestMapping(method = {RequestMethod.POST})
    public ResponseEntity<String> get(@RequestBody final String payloadMany) throws IOException {
        System.out.println(payloadMany);
        ObjectMapper objectMapper = new ObjectMapper();
        subcribers = objectMapper.readValue(payloadMany,Subcribers.class);
        query();
        if(subcribers.getCustom_fields().getType().compareToIgnoreCase(TypeMessage.TEXT)==0) {
            sendTextMessage();
            postRequestHttp();
        }
        if(subcribers.getCustom_fields().getType().compareToIgnoreCase(TypeMessage.IMAGE)==0) {
            sendImageMessage();
            postRequestHttp();
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    private void query() {
        String genderS = subcribers.getCustom_fields().getGender();
        if (genderS.compareToIgnoreCase(MALE)==0) { Gender =true; }
        else { Gender =false; }
        influencers = influencerService.QueryAll(Integer.valueOf(subcribers.getCustom_fields().getAgeFrom()),
                Integer.valueOf(subcribers.getCustom_fields().getAgeTo()),
                Integer.valueOf(subcribers.getCustom_fields().getFollowerFrom()),
                Integer.valueOf(subcribers.getCustom_fields().getFollowerTo()),
                Gender,
                subcribers.getCustom_fields().getLocation());
    }
    private void sendImageMessage() {
        Data data = new Data();
        Message message = new Message();
        Content content = new Content();
        message.setType(TypeMessage.IMAGE);
        message.setUrl(subcribers.getProfile_pic());
        Message[] messages = new Message[] { message };
        content.setMessages(messages);
        data.setContent(content);
        data.setVersion("v2");
        responseFormat.setData(data);
        responseFormat.setSubscriberId(subcribers.getId());
    }
    private void sendTextMessage() {
        if(!influencers.isEmpty()) {
            for (int i=0;i<influencers.size();++i) {
                String gender;
                if(influencers.get(i).getGender()) { gender = MALE; }
                else { gender = FEMALE; }
                Result = Result + "ID : "+influencers.get(i).getId()
                        +"\nUsername : "+influencers.get(i).getUsername()
                        +"\nFullname : "+influencers.get(i).getFullname()
                        +"\nAge : "+influencers.get(i).getAge()
                        +"\nGender : "+gender
                        +"\nNumber of Follower : "+influencers.get(i).getFollowers()
                        +"\nLocation : "+influencers.get(i).getLocation()
                        +"\n";
            }
        }
        else {
            Result = "empty";
        }
        Data data = new Data();
        Message message = new Message();
        Content content = new Content();
        message.setType(TypeMessage.TEXT);
        message.setText(Result);
        Message[] messages = new Message[] { message };
        content.setMessages(messages);
        data.setContent(content);
        data.setVersion("v2");
        responseFormat.setData(data);
        responseFormat.setSubscriberId(subcribers.getId());
    }
    private void postRequestHttp() throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        json = objectMapper.writeValueAsString(responseFormat);
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(Url);
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.addHeader("Authorization",author);
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = client.execute(httpPost);
        client.close();
    }
}

