package springframework.findsomeone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InfluencerService implements ImplementInfluencer {

    @Autowired
    private  InfluencerRepository repository;

    @Override
    public List<Influencer> AddInfluencer() {
        return null;
    }

    @Override
    public List<Influencer> QueryById(int id) {
        List<Influencer> influencer = repository.findById(id);
        return influencer;
    }
    @Override
    public List<Influencer> QueryByAge(int age) {
        List<Influencer> influencer = repository.findByAge(age);
        return influencer;
    }

    @Override
    public List<Influencer> Query(int age,boolean gender,int followers,String location) {
        List<Influencer> influencers = repository.findByAgeAndGenderAndFollowersAndLocation(age, gender, followers, location);
        return influencers;
    }

    @Override
    public List<Influencer> QueryAll(int ageFrom, int ageTo, int followersFrom, int followersTo, boolean gender, String location) {
        List<Influencer> influencers = repository.findByAgeBetweenAndFollowersBetweenAndGenderAndLocation(ageFrom,ageTo,followersFrom,followersTo,gender,location);
        return influencers;
    }

    @Override
    public int Count() {
        return repository.countAllBy();
    }
}
