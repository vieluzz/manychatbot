package Config;

public class CustomFields
{
    private String followerFrom;

    private String followerTo;

    private String location;

    private String ageFrom;

    private String ageTo;

    private String gender;

    //private String userID;

    private String type;

//    public String getUserID() {
//        return userID;
//    }
//
//    public void setUserID(String userID) {
//        this.userID = userID;
//    }

    public String getFollowerFrom ()
    {
        return followerFrom;
    }

    public void setFollowerFrom (String followerFrom)
    {
        this.followerFrom = followerFrom;
    }

    public String getFollowerTo ()
    {
        return followerTo;
    }

    public void setFollowerTo (String followerTo)
    {
        this.followerTo = followerTo;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getAgeFrom ()
    {
        return ageFrom;
    }

    public void setAgeFrom (String ageFrom)
    {
        this.ageFrom = ageFrom;
    }

    public String getAgeTo ()
    {
        return ageTo;
    }

    public void setAgeTo (String ageTo)
    {
        this.ageTo = ageTo;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ followerFrom = "+followerFrom+", location = "+location+", age = "+ageFrom+", gender = "+gender+"]";
    }
}

