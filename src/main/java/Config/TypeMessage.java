package Config;

public class TypeMessage {
    public static final String TEXT = "text";
    public static final String IMAGE = "image";
    public static final String VIDEO = "video";
    public static final String AUDIO = "audio";
    public static final String FILES = "file";
    public static final String GALLERY_CARDS = "cards";
    public static final String LISTS = "list";
}
