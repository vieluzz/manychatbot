package Config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseFormat {

    private Data data;

    @JsonProperty("subscriber_id")
    private String subscriberId;

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public String getSubscriberId()
    {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId)
    {
        this.subscriberId = subscriberId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", subscriberId = "+ subscriberId +"]";
    }
}
