package Config;

public class Subcribers {
    private CustomFields custom_fields;

    private String live_chat_url;

    private String status;

    private String locale;

    private String subscribed;

    private String is_followup_enabled;

    private String id;

    private String last_interaction;

    private String first_name;

    private String timezone;

    private String name;

    private String last_growth_tool;

    private String last_name;

    private String page_id;

    private String gender;

    private String profile_pic;

    private String language;

    private String last_seen;

    private String key;

    private String last_input_text;

    public CustomFields getCustom_fields ()
    {
        return custom_fields;
    }

    public void setCustom_fields (CustomFields custom_fields)
    {
        this.custom_fields = custom_fields;
    }

    public String getLive_chat_url ()
    {
        return live_chat_url;
    }

    public void setLive_chat_url (String live_chat_url)
    {
        this.live_chat_url = live_chat_url;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getLocale ()
    {
        return locale;
    }

    public void setLocale (String locale)
    {
        this.locale = locale;
    }

    public String getSubscribed ()
    {
        return subscribed;
    }

    public void setSubscribed (String subscribed)
    {
        this.subscribed = subscribed;
    }

    public String getIs_followup_enabled ()
    {
        return is_followup_enabled;
    }

    public void setIs_followup_enabled (String is_followup_enabled)
    {
        this.is_followup_enabled = is_followup_enabled;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLast_interaction ()
    {
        return last_interaction;
    }

    public void setLast_interaction (String last_interaction)
    {
        this.last_interaction = last_interaction;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getTimezone ()
    {
        return timezone;
    }

    public void setTimezone (String timezone)
    {
        this.timezone = timezone;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getLast_growth_tool ()
    {
        return last_growth_tool;
    }

    public void setLast_growth_tool (String last_growth_tool)
    {
        this.last_growth_tool = last_growth_tool;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getPage_id ()
    {
        return page_id;
    }

    public void setPage_id (String page_id)
    {
        this.page_id = page_id;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getProfile_pic ()
    {
        return profile_pic;
    }

    public void setProfile_pic (String profile_pic)
    {
        this.profile_pic = profile_pic;
    }

    public String getLanguage ()
    {
        return language;
    }

    public void setLanguage (String language)
    {
        this.language = language;
    }

    public String getLast_seen ()
    {
        return last_seen;
    }

    public void setLast_seen (String last_seen)
    {
        this.last_seen = last_seen;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    public String getLast_input_text ()
    {
        return last_input_text;
    }

    public void setLast_input_text (String last_input_text)
    {
        this.last_input_text = last_input_text;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [custom_fields = "+custom_fields+", live_chat_url = "+live_chat_url+", status = "+status+", locale = "+locale+", subscribed = "+subscribed+", is_followup_enabled = "+is_followup_enabled+", id = "+id+", last_interaction = "+last_interaction+", first_name = "+first_name+", timezone = "+timezone+", name = "+name+", last_growth_tool = "+last_growth_tool+", last_name = "+last_name+", page_id = "+page_id+", gender = "+gender+", profile_pic = "+profile_pic+", language = "+language+", last_seen = "+last_seen+", key = "+key+", last_input_text = "+last_input_text+"]";
    }
}
