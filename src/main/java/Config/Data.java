package Config;

public class Data {
    private Content content;

    private String version;

    private CustomFields manyChat;

    public Content getContent ()
    {
        return content;
    }

    public void setContent (Content content)
    {
        this.content = content;
    }

    public String getVersion ()
    {
        return version;
    }

    public void setVersion (String version)
    {
        this.version = version;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [content = "+content+", version = "+version+"]";
    }
}
