package Config;

public class Content {
    private Message[] messages;

    public Message[] getMessages ()
    {
        return messages;
    }

    public void setMessages (Message[] messages)
    {
        this.messages = messages;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [messages = "+messages+"]";
    }
}
