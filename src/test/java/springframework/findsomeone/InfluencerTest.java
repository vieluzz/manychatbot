package springframework.findsomeone;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class InfluencerTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    InfluencerRepository influencerRepository;

    public void test() {
        Influencer influencer = new Influencer("PVL","luc",true,200,20,"QNG");
        entityManager.persist(influencer);
        entityManager.flush();
        Influencer influencer1 = (Influencer) influencerRepository.findByAge(influencer.getAge());
        assertThat(influencer1.getFullname()).isEqualTo(influencer.getFullname());
    }
}
